package com.example.aulaprogramacaomobile;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

public class ActivityAutoria extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(lerTema()){
            setTheme(R.style.DarkMode);
        }else{
            setTheme(R.style.AppTheme);
        }
        
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autoria);

        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        setTitle(getString(R.string.autoria));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean lerTema(){
        SharedPreferences shared = getSharedPreferences(ActivityListagem.ARQUIVO, Context.MODE_PRIVATE);

        boolean darkMode = shared.getBoolean(ActivityListagem.THEME, false);

        return darkMode;
    }
}