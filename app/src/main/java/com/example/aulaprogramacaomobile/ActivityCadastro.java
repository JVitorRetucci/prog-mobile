package com.example.aulaprogramacaomobile;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aulaprogramacaomobile.modelos.Alimento;
import com.example.aulaprogramacaomobile.modelos.Refeicao;
import com.example.aulaprogramacaomobile.modelos.Tipo;
import com.example.aulaprogramacaomobile.persistencia.RefeicaoDatabase;
import com.example.aulaprogramacaomobile.utils.UtilsDate;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ActivityCadastro extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{

    private EditText editTextData;
    private Calendar calendarDataRef;
    private EditText editTextMinuto;
    private EditText editTextHora;
    private EditText editTextNomeAlimento;
    private EditText editTextQuantidade;
    private TextView textViewInfoAlimentos;
    private CheckBox cbBebida;
    private RadioGroup rgNutrientes;
    private Spinner spinnerTipoRefeicao;
    private List<Tipo> lista = new ArrayList<>();
    private Refeicao ref = null;
    private ArrayList<Alimento> alimentos;
    private String stringListaAlimentos = "";

    public static final String REFEICAO = "REFEICAO";
    public static final String MODO = "MODO";
    public static final int NOVO = 1;
    public static final int EDITAR = 2;
    private int modo;

    Date final_data;
    int final_hora;
    int final_minuto;
    int tipo_refeicao;

    public static void novaRefeicao(AppCompatActivity activity){
        Intent intent = new Intent(activity, ActivityCadastro.class);
        intent.putExtra(MODO, NOVO);
        activity.startActivityForResult(intent, NOVO);
    }

    public static void alterarRefeicao(AppCompatActivity activity, int refId){
        Intent intent = new Intent(activity, ActivityCadastro.class);

        intent.putExtra(MODO, EDITAR);
        intent.putExtra(REFEICAO, refId);

        activity.startActivityForResult(intent, EDITAR);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(lerTema()){
            setTheme(R.style.DarkMode);
        }else{
            setTheme(R.style.AppTheme);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        editTextData = findViewById(R.id.editTextData);
        editTextMinuto = findViewById(R.id.editTextMinuto);
        editTextHora = findViewById(R.id.editTextHora);
        editTextNomeAlimento = findViewById(R.id.editTextNomeAlimento);
        editTextQuantidade = findViewById(R.id.editTextQuantidade);
        textViewInfoAlimentos = findViewById(R.id.textViewInfoAlimentos);
        cbBebida = findViewById(R.id.checkBoxBebida);
        rgNutrientes = findViewById(R.id.radioGroupNutri);
        spinnerTipoRefeicao = findViewById(R.id.spinnerTipoRefeicao);
        alimentos = new ArrayList<>();
        calendarDataRef = Calendar.getInstance();

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        editTextData.setFocusable(false);
        editTextData.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                /* Para fazer o DatePicker aparecer em modo Spinner e não Calendar
                   para SDKs iguais ou maiores que o API Level 21 foi utilizado um
                   estilo customizado, que está na pasta values-v21.

                   Versões anteriores já aparecem em modo Spinner por padrão.
                 */
                DatePickerDialog picker;

                if(lerTema()){
                    picker = new DatePickerDialog(ActivityCadastro.this,
                            AlertDialog.THEME_DEVICE_DEFAULT_DARK,
                            ActivityCadastro.this,
                            calendarDataRef.get(Calendar.YEAR),
                            calendarDataRef.get(Calendar.MONTH),
                            calendarDataRef.get(Calendar.DAY_OF_MONTH));
                }else{
                    picker = new DatePickerDialog(ActivityCadastro.this,
                            ActivityCadastro.this,
                            calendarDataRef.get(Calendar.YEAR),
                            calendarDataRef.get(Calendar.MONTH),
                            calendarDataRef.get(Calendar.DAY_OF_MONTH));
                }

                picker.show();
            }
        });

        if(bundle != null){
            modo = bundle.getInt(MODO, NOVO);

            if(modo == NOVO){
                setTitle(R.string.nova_refeicao);
            }else{
                popularCampos();
            }
        }

        popularSpinner();
    }

    public void popularCampos(){
        setTitle(getString(R.string.editar_refeicao));

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                RefeicaoDatabase database = RefeicaoDatabase.getDatabase(ActivityCadastro.this);
                ref = database.refeicaoDAO().queryForId(getIntent().getExtras().getInt(REFEICAO));

                alimentos.addAll(database.alimentoDAO().queryAllByRef(ref.getId()));
                for(int i=0; i < alimentos.size(); i++){
                    stringListaAlimentos += alimentos.get(i).toString();
                }

                ActivityCadastro.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        editTextData.setText(UtilsDate.formatDate(ActivityCadastro.this, ref.getData()));
                        calendarDataRef.setTime(ref.getData());
                        editTextHora.setText(String.valueOf(ref.getHora()));
                        editTextMinuto.setText(String.valueOf(ref.getMinutos()));
                        cbBebida.setChecked(ref.isTeveBebida());
                        textViewInfoAlimentos.setText(stringListaAlimentos);
                        editTextData.requestFocus();
                    }
                });
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.cadastro_opcoes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menuItemSalvar:
                salvar();
                return true;

            case R.id.menuItemLimpar:
                limpar();
                return true;

            case android.R.id.home:
                this.onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean lerTema(){
        SharedPreferences shared = getSharedPreferences(ActivityListagem.ARQUIVO, Context.MODE_PRIVATE);

        boolean darkMode = shared.getBoolean(ActivityListagem.THEME, false);

        return darkMode;
    }

    private void popularSpinner(){
        /*lista.add("");
        lista.add(getString(R.string.cafe_da_manha));
        lista.add(getString(R.string.lanche_manha));
        lista.add(getString(R.string.almoco));
        lista.add(getString(R.string.cafe_da_tarde));
        lista.add(getString(R.string.janta));
        lista.add(getString(R.string.lanche_noite));*/
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                RefeicaoDatabase database = RefeicaoDatabase.getDatabase(ActivityCadastro.this);

                lista = database.tipoDAO().queryAll();

                ActivityCadastro.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        SpinnerAdapter adapter = new SpinnerAdapter(ActivityCadastro.this, lista);
                        spinnerTipoRefeicao.setAdapter(adapter);
                    }
                });
            }
        });
    }

    private void transformaArrayList(){
        stringListaAlimentos += alimentos.get(alimentos.size()-1);
    }

    public void limpar(){
        editTextData.setText(null);
        editTextHora.setText(null);
        editTextMinuto.setText(null);
        spinnerTipoRefeicao.setSelection(0);                    //Reinicia o Spinner
        cbBebida.setChecked(false);
        editTextNomeAlimento.setText(null);
        rgNutrientes.clearCheck();                          //Reinicia o Radio Group
        editTextQuantidade.setText(null);
        stringListaAlimentos = "";
        alimentos.clear();
        textViewInfoAlimentos.setText(R.string.informacoes_alimentos);
        editTextData.requestFocus();
        Toast.makeText(this, R.string.mensagemCamposLimpos, Toast.LENGTH_SHORT).show();

    }

    public void salvar() {
        Date data = calendarDataRef.getTime();
        String hora = editTextHora.getText().toString();
        String minuto = editTextMinuto.getText().toString();
        tipo_refeicao = -1;

        if((data.toString() == null || data.toString().trim().isEmpty())){
            Toast.makeText(this, R.string.erro_dia, Toast.LENGTH_SHORT).show();
            editTextData.requestFocus();
            return;
        }

        if(hora == null || hora.trim().isEmpty()||((Integer.parseInt(hora) > 23))){
            Toast.makeText(this, R.string.hora_erro, Toast.LENGTH_SHORT).show();
            editTextHora.requestFocus();
            return;
        }

        if(minuto == null || minuto.trim().isEmpty()||((Integer.parseInt(minuto) > 60)||(Integer.parseInt(minuto) < 0))){
            Toast.makeText(this, R.string.minuto_erro, Toast.LENGTH_SHORT).show();
            editTextMinuto.requestFocus();
            return;
        }

        tipo_refeicao = testarSpinner(spinnerTipoRefeicao);

        if(tipo_refeicao == -1){
            Toast.makeText(this, R.string.erro_tipo_refeicao, Toast.LENGTH_SHORT).show();
            rgNutrientes.requestFocus();
            return;
        }

        if(alimentos.isEmpty()){
            Toast.makeText(this, R.string.adicione_alimento, Toast.LENGTH_SHORT).show();
            editTextNomeAlimento.requestFocus();
            return;
        }

        final_data = data;
        final_hora = Integer.parseInt(hora);
        final_minuto = Integer.parseInt(minuto);

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                RefeicaoDatabase database = RefeicaoDatabase.getDatabase(ActivityCadastro.this);

                if(modo == NOVO) {
                    ref = new Refeicao(tipo_refeicao, final_data, final_hora, final_minuto, cbBebida.isChecked());
                    //ref = new Refeicao(final_dia, final_mes, final_hora, final_minuto, cbBebida.isChecked());

                    long refId = database.refeicaoDAO().insert(ref);
                    setAlimentosRefIds((int) refId, database);
                    inserirAlimentos(alimentos, database);
                }else{
                    ref.setTipoId(tipo_refeicao);
                    ref.setData(final_data);
                    ref.setHora(final_hora);
                    ref.setMinutos(final_minuto);
                    ref.setTeveBebida(cbBebida.isChecked());

                    database.refeicaoDAO().update(ref);
                    setAlimentosRefIds(ref.getId(), database);
                    inserirAlimentos(alimentos, database);
                }
            }
        });

        Toast.makeText(this, R.string.salvo, Toast.LENGTH_SHORT).show();
        finalizar();
    }

    private void inserirAlimentos(ArrayList<Alimento> alimentos, RefeicaoDatabase database){
        for(Alimento alimento:alimentos){
            database.alimentoDAO().insert(alimento);
        }
    }

    private void setAlimentosRefIds(int refId, RefeicaoDatabase database){
        if(modo == EDITAR){
            database.alimentoDAO().queryDeleteALlByRef(refId);
        }

        for(Alimento alimento:alimentos){
            alimento.setRefId(refId);
            database.alimentoDAO().update(alimento);
        }
    }

    public void adicionarAlimento(View view){
        String nutriente;
        String nome = editTextNomeAlimento.getText().toString();
        String qtd_s = editTextQuantidade.getText().toString();

        if(nome == null || nome.trim().isEmpty()){
            Toast.makeText(this, R.string.alimento_erro, Toast.LENGTH_SHORT).show();
            editTextNomeAlimento.requestFocus();
            return;
        }

        if(qtd_s == null || qtd_s.trim().isEmpty() || Float.parseFloat(editTextQuantidade.getText().toString()) < 0){
            Toast.makeText(this, R.string.quantidade_erro, Toast.LENGTH_SHORT).show();
            editTextQuantidade.requestFocus();
            return;
        }

        //Verificação RadioButtons
        switch (rgNutrientes.getCheckedRadioButtonId()){

            case R.id.radioButtonCarbo:
                nutriente = getString(R.string.carbo);
                break;

            case  R.id.radioButtonProt:
                nutriente = getString(R.string.prot);
                break;

            case R.id.radioButtonLipi:
                nutriente = getString(R.string.lipi);
                break;

            default:
                Toast.makeText(this, R.string.erro_nutri, Toast.LENGTH_SHORT).show();
                rgNutrientes.requestFocus();
                return;

        }

        Float qtd = Float.parseFloat(editTextQuantidade.getText().toString());
        alimentos.add(new Alimento(-1, nome, nutriente, qtd));
        transformaArrayList();
        textViewInfoAlimentos.setText(getText(R.string.informacoes_alimentos) + "\n" + stringListaAlimentos);

        editTextNomeAlimento.setText(null);
        rgNutrientes.clearCheck();                          //Reinicia o Radio Group
        editTextQuantidade.setText(null);
    }

    public void limpaAlimentos(View view){
        /*RefeicaoDatabase database = RefeicaoDatabase.getDatabase(this);

        if(ref == null){
            database.alimentoDAO().queryDeleteALlByRef(ref.getId());
        }*/

        editTextNomeAlimento.setText(null);
        rgNutrientes.clearCheck();                          //Reinicia o Radio Group
        editTextQuantidade.setText(null);
        textViewInfoAlimentos.setText(R.string.informacoes_alimentos);
        stringListaAlimentos = "";
        alimentos.clear();
    }

    private int testarSpinner(Spinner spinner){
        Tipo tipo_refeicao = (Tipo) spinner.getSelectedItem();

        if(tipo_refeicao == null) return -1;
        return tipo_refeicao.getId();
    }

    private void finalizar(){
        setResult(Activity.RESULT_OK);

        finish();
    }

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_CANCELED);
        super.onBackPressed();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        calendarDataRef.set(year, month, dayOfMonth);

        String textoData = UtilsDate.formatDate(this, calendarDataRef.getTime());

        editTextData.setText(textoData);
    }
}
