package com.example.aulaprogramacaomobile;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.aulaprogramacaomobile.modelos.Alimento;
import com.example.aulaprogramacaomobile.modelos.Refeicao;
import com.example.aulaprogramacaomobile.persistencia.RefeicaoDatabase;
import com.example.aulaprogramacaomobile.utils.UtilsGUI;

import java.util.ArrayList;
import java.util.List;

public class ActivityListagem extends AppCompatActivity {

    public static final String ARQUIVO = "com.exemple.aulaprogramacaomobile.MODE";
    public static final String THEME = "THEME";
    public boolean darkMode = false;
    private MenuItem menuItemMode;

    private ListView listViewRefeicoes;
    private RefeicaoAdapter adapter;
    private ArrayList<Refeicao> refeicoes;
    private List<String> nomesTipos;

    private ActionMode actionMode;
    public int posicaoSelecionada = -1;
    private View viewSelecionada;

    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {

            MenuInflater inflate = mode.getMenuInflater();
            inflate.inflate(R.menu.principal_item_selecionado, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()){
                case R.id.menuItemAlterar:
                    alterarRefeicao();
                    mode.finish();
                    return true;

                case R.id.menuItemExcluir:
                    excluirRefeicao();
                    mode.finish();
                    return true;

                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            if(viewSelecionada != null){
                viewSelecionada.setBackgroundColor(Color.TRANSPARENT);
            }

            actionMode = null;
            viewSelecionada = null;

            listViewRefeicoes.setEnabled(true);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(lerTema()){
            setTheme(R.style.DarkMode);
        }else{
            setTheme(R.style.AppTheme);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listagem);

        listViewRefeicoes = findViewById(R.id.listViewRefeicoes);
        listViewRefeicoes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Refeicao refeicao = (Refeicao) listViewRefeicoes.getItemAtPosition(position);

                Toast.makeText(getApplicationContext(), nomesTipos.get(refeicao.getTipoId()-1) + " - " + refeicao.getData() + getString(R.string.got_clicked), Toast.LENGTH_SHORT).show();
            }
        });

        listViewRefeicoes.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        listViewRefeicoes.setOnItemLongClickListener(
                new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                        if(actionMode != null) {
                            return false;
                        }

                        posicaoSelecionada = position;
                        view.setBackgroundColor(Color.GRAY);
                        viewSelecionada = view;
                        listViewRefeicoes.setEnabled(false);
                        actionMode = startActionMode(mActionModeCallback);
                        return true;
                    }
                }
        );

        popularLista();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.listagem_opcoes, menu);
        menuItemMode = menu.findItem(R.id.menuItemMode);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item;
        item = menu.findItem(R.id.menuItemMode);
        item.setChecked(darkMode);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.menuItemNovo:
                abrirAdicionarRefeicao();
                return true;

            case R.id.menuItemSobre:
                abrirSobre();
                return true;

            case R.id.menuItemAddRef:
                abrirTiposRef();
                return true;

            case R.id.menuItemMode:
                salvarTema(!darkMode);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean lerTema(){
        SharedPreferences shared = getSharedPreferences(ARQUIVO, Context.MODE_PRIVATE);

        darkMode = shared.getBoolean(THEME, false);

        //mudarTema();

        return darkMode;
    }

    public void salvarTema(boolean novoValor){
        SharedPreferences shared = getSharedPreferences(ARQUIVO, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();

        editor.putBoolean(THEME, novoValor);
        editor.commit();
        darkMode = novoValor;

        mudarTema();
    }

    public void mudarTema(){

        if(darkMode){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            restartApp();
        }else{
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            restartApp();
        }
        menuItemMode.setChecked(darkMode);
        //item.setChecked(!item.isChecked());
    }

    public void restartApp(){
        Intent i = new Intent(getApplicationContext(), ActivityListagem.class);
        startActivity(i);
        finish();
    }

    private void popularLista(){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                RefeicaoDatabase database = RefeicaoDatabase.getDatabase(ActivityListagem.this);

                nomesTipos = database.tipoDAO().queryAllNames();

                refeicoes = new ArrayList<>();
                refeicoes.addAll(database.refeicaoDAO().queryAll());

                ActivityListagem.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        RefeicaoDatabase database = RefeicaoDatabase.getDatabase(ActivityListagem.this);
                        ArrayList<List<Alimento>> alimentoLista = new ArrayList<>();

                        for (Refeicao ref:refeicoes) {
                            alimentoLista.add(database.alimentoDAO().queryAllByRef(ref.getId()));
                        }

                        Log.d("TAMANHOS", "ref: " + refeicoes.size() + " alim: " + alimentoLista.size());
                        adapter = new RefeicaoAdapter(ActivityListagem.this,  refeicoes, alimentoLista);
                        listViewRefeicoes.setAdapter(adapter);
                    }
                });
            }
        });

    }

    public void abrirSobre(){
        Intent intent = new Intent(this, ActivityAutoria.class);
        startActivity(intent);
    }

    public void abrirTiposRef(){
        Intent intent = new Intent(this, ListaTiposActivity.class);
        startActivity(intent);
    }

    private void alterarRefeicao() {
        Refeicao refeicao = refeicoes.get(posicaoSelecionada);
        ActivityCadastro.alterarRefeicao(this, refeicao.getId());
    }

    private void excluirRefeicao(){
        String message = getString(R.string.confirm_delete_meal);

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        AsyncTask.execute(new Runnable() {
                            @Override
                            public void run() {
                                RefeicaoDatabase database = RefeicaoDatabase.getDatabase(ActivityListagem.this);

                                database.refeicaoDAO().delete(refeicoes.get(posicaoSelecionada));
                                refeicoes.remove(posicaoSelecionada);
                            }
                        });
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        UtilsGUI.confirmaAcao(this, message, listener);
        adapter.notifyDataSetChanged();
        posicaoSelecionada = -1;
    }

    public void abrirAdicionarRefeicao(){
        ActivityCadastro.novaRefeicao(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if(resultCode == Activity.RESULT_OK){
            refeicoes.clear();
            popularLista();
            adapter.notifyDataSetChanged();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
