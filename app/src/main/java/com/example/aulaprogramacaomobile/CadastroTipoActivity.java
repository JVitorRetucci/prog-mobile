package com.example.aulaprogramacaomobile;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.example.aulaprogramacaomobile.modelos.Tipo;
import com.example.aulaprogramacaomobile.persistencia.RefeicaoDatabase;

public class CadastroTipoActivity extends AppCompatActivity {
    public static final String TIPO = "TIPO";
    public static final String MODO = "MODO";
    public static final int NOVO = 1;
    public static final int EDITAR = 2;

    private EditText editTextNome;
    private EditText editTextDescricao;

    private int modo;
    private Tipo tipo;
    private String nome;
    private String descricao;

    public static void novoTipo(AppCompatActivity activity){
        Intent intent = new Intent(activity, CadastroTipoActivity.class);

        intent.putExtra(MODO, NOVO);

        activity.startActivityForResult(intent, NOVO);
    }

    public static void alterarTipo(AppCompatActivity activity, int tipoId){
        Intent intent = new Intent(activity, CadastroTipoActivity.class);

        intent.putExtra(MODO, EDITAR);
        intent.putExtra(TIPO, tipoId);

        activity.startActivityForResult(intent, EDITAR);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (lerTema()) {
            setTheme(R.style.DarkMode);
        } else {
            setTheme(R.style.AppTheme);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_tipo);

        editTextNome = findViewById(R.id.editTextTipoNome);
        editTextDescricao = findViewById(R.id.editTextTipoDescricao);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        configModoAbrir();

        setTitle(getString(R.string.meal_type));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.tipos_cadastro_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.menuItemSalvarTipo:
                salvaTipo();
                return true;

            case R.id.menuItemLimparTipo:
                limpaTipo();

            case android.R.id.home:
                this.onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void configModoAbrir(){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                Intent intent = getIntent();
                Bundle bundle = intent.getExtras();

                if(bundle != null){
                    RefeicaoDatabase database = RefeicaoDatabase.getDatabase(CadastroTipoActivity.this);

                    modo = bundle.getInt(MODO, NOVO);
                    tipo = database.tipoDAO().queryForId(bundle.getInt(TIPO));
                    if(modo == NOVO){
                        setTitle(R.string.new_meal_type);
                    }else{
                        editTextNome.setText(tipo.getNome());
                        editTextDescricao.setText(tipo.getDescricao());
                    }
                }
            }
        });
    }



    private void salvaTipo(){
        nome = editTextNome.getText().toString().trim();
        descricao = editTextDescricao.getText().toString().trim();

        if(nome.equals("") || nome.equals(null)){
            Toast.makeText(this, R.string.erro_nome_tipo, Toast.LENGTH_SHORT).show();
            editTextNome.requestFocus();
            return;
        }

        if(nome.matches(".*\\d.*")){
            Toast.makeText(this, "A refeição deve conter apenas caracteres", Toast.LENGTH_SHORT).show();
            editTextNome.requestFocus();
            return;
        }

        if (descricao.equals("")){
            descricao = null;
        }

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                RefeicaoDatabase database = RefeicaoDatabase.getDatabase(CadastroTipoActivity.this);

                if(modo == NOVO){
                    tipo = new Tipo(nome, descricao);
                    database.tipoDAO().insert(tipo);
                }else{
                    tipo.setNome(nome);
                    tipo.setDescricao(descricao);
                    database.tipoDAO().update(tipo);
                }
            }
        });

        Toast.makeText(this, R.string.salvo, Toast.LENGTH_SHORT).show();
        finalizar();
    }

    private void limpaTipo(){
        editTextNome.setText("");
        editTextDescricao.setText("");
    }

    private void finalizar(){
        setResult(Activity.RESULT_OK);
        finish();
    }

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_CANCELED);
        super.onBackPressed();
    }

    public boolean lerTema(){
        SharedPreferences shared = getSharedPreferences(ActivityListagem.ARQUIVO, Context.MODE_PRIVATE);

        return shared.getBoolean(ActivityListagem.THEME, false);
    }
}