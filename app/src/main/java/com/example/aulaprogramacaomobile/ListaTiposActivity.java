package com.example.aulaprogramacaomobile;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.example.aulaprogramacaomobile.modelos.Tipo;
import com.example.aulaprogramacaomobile.persistencia.RefeicaoDatabase;

import java.sql.Ref;
import java.util.List;

public class ListaTiposActivity extends AppCompatActivity {
    private List<Tipo> tipos;
    private TiposAdapter adapter;
    private ListView listViewTipos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (lerTema()) {
            setTheme(R.style.DarkMode);
        } else {
            setTheme(R.style.AppTheme);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_tipos);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        listViewTipos = findViewById(R.id.listViewTiposRefeicoes);

        setTitle(getString(R.string.tipos_refeicao));

        populaListaTipos();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.tipos_opcoes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){
            case R.id.menuItemAddTipo:
                abrirAddNovoTipo();
                return true;

            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void populaListaTipos(){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                RefeicaoDatabase database = RefeicaoDatabase.getDatabase(ListaTiposActivity.this);

                tipos = database.tipoDAO().queryAll();

                ListaTiposActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter = new TiposAdapter(ListaTiposActivity.this, tipos);
                        listViewTipos.setAdapter(adapter);
                    }
                });
            }
        });
    }

    private void abrirAddNovoTipo(){
        CadastroTipoActivity.novoTipo(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if(resultCode == Activity.RESULT_OK){
            tipos.clear();
            populaListaTipos();
            adapter.notifyDataSetChanged();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public boolean lerTema(){
        SharedPreferences shared = getSharedPreferences(ActivityListagem.ARQUIVO, Context.MODE_PRIVATE);

        boolean darkMode = shared.getBoolean(ActivityListagem.THEME, false);

        return darkMode;
    }
}