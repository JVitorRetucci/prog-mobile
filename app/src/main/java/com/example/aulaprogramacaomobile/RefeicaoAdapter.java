package com.example.aulaprogramacaomobile;

import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.aulaprogramacaomobile.modelos.Alimento;
import com.example.aulaprogramacaomobile.modelos.Refeicao;
import com.example.aulaprogramacaomobile.persistencia.RefeicaoDatabase;

import java.util.List;

public class RefeicaoAdapter extends BaseAdapter {

    Context context;
    List<Refeicao> refeicoes;
    List<List<Alimento>> alimentosLista;

    public RefeicaoAdapter(Context context, List<Refeicao> refeicoes, List<List<Alimento>> alimentosLista) {
        this.context = context;
        this.refeicoes = refeicoes;
        this.alimentosLista = alimentosLista;
    }

    public String retornaStringAlimento(List<Alimento> alimentos){
        String alimentosString = "";

        for(Alimento alimento : alimentos){
            alimentosString += alimento.toString();
        }
        return alimentosString;
    }

    private static class RefeicaoHolder{
        public TextView textViewInfos;
    }

    @Override
    public int getCount() {
        return refeicoes.size();
    }

    @Override
    public Object getItem(int position) {
        return refeicoes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        RefeicaoHolder holder;
        String teveBebida;

        if(view == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.lista_refeicoes, viewGroup, false);

            holder = new RefeicaoHolder();

            holder.textViewInfos = view.findViewById(R.id.textViewInfos);

            view.setTag(holder);
        }else{
            holder = (RefeicaoHolder) view.getTag();
        }

        if(refeicoes.get(i).isTeveBebida()){
            teveBebida = context.getString(R.string.teve_bebida);
        }else {
            teveBebida = context.getString(R.string.nao_teve_bebida);
        }

        String tipo_refeicao = getNomeTipo(refeicoes.get(i).getTipoId());

        holder.textViewInfos.setText(context.getString(R.string.show_refeicao) + tipo_refeicao + " - " + refeicoes.get(i).getData() + "):\n"
                                    + teveBebida + "\n"
                                    + context.getString(R.string.show_hora) + String.format("%02d", refeicoes.get(i).getHora()) + ":" + String.format("%02d", refeicoes.get(i).getMinutos()) + "\n"
                                    + context.getString(R.string.show_alimentos)
                                    + retornaStringAlimento(alimentosLista.get(i))
        );

        return view;
    }

    private String getNomeTipo(int tipoId){
        String nome = new String();

        RefeicaoDatabase database = RefeicaoDatabase.getDatabase(context);

        nome = database.tipoDAO().getNameById(tipoId);

        return nome;
    }
}
