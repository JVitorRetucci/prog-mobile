package com.example.aulaprogramacaomobile;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.aulaprogramacaomobile.modelos.Tipo;

import java.util.List;

public class SpinnerAdapter extends BaseAdapter {
    Context context;
    List<Tipo> tipos;
    //List<String> tipos;

    //public SpinnerAdapter(Context context, List<String> tipos) {
    public SpinnerAdapter(Context context, List<Tipo> tipos) {
        this.context = context;
        this.tipos = tipos;
    }

    private static class TiposHolder{
        public TextView textViewInfos;
    }

    @Override
    public int getCount() {
        return tipos.size();
    }

    @Override
    public Object getItem(int position) {
        return tipos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        SpinnerAdapter.TiposHolder holder;

        if(view == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.lista_refeicoes, viewGroup, false);

            holder = new SpinnerAdapter.TiposHolder();

            holder.textViewInfos = view.findViewById(R.id.textViewInfos);

            view.setTag(holder);
        }else{
            holder = (SpinnerAdapter.TiposHolder) view.getTag();
        }

        holder.textViewInfos.setText(tipos.get(i).getNome());
        //holder.textViewInfos.setText(tipos.get(i));

        return view;
    }
}
