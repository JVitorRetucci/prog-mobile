package com.example.aulaprogramacaomobile.modelos;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(foreignKeys = @ForeignKey(entity = Refeicao.class,
            parentColumns = "id",
            childColumns = "refId",
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE)
    )
public class Alimento implements Parcelable {
    @PrimaryKey(autoGenerate = true)
    private int id;

    private int refId;

    private String nome;
    private String nutriente;
    private Float qtd;

    public Alimento(int refId, String nome, String nutriente, Float qtd){
        this.refId = refId;
        this.nome = nome;
        this.nutriente = nutriente;
        this.qtd = qtd;
    }

    protected Alimento(Parcel in) {
        nome = in.readString();
        nutriente = in.readString();
        if (in.readByte() == 0) {
            qtd = null;
        } else {
            qtd = in.readFloat();
        }
    }

    public static final Creator<Alimento> CREATOR = new Creator<Alimento>() {
        @Override
        public Alimento createFromParcel(Parcel in) {
            return new Alimento(in);
        }

        @Override
        public Alimento[] newArray(int size) {
            return new Alimento[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRefId() {
        return refId;
    }

    public void setRefId(int refId) {
        this.refId = refId;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNutriente() {
        return nutriente;
    }

    public void setNutriente(String nutriente) {
        this.nutriente = nutriente;
    }

    public Float getQtd() {
        return qtd;
    }

    public void setQtd(Float qtd) {
        this.qtd = qtd;
    }

    @Override
    public String toString() {
        return nome + ", " + nutriente + ", " + qtd + "\n";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nome);
        dest.writeString(nutriente);
        if (qtd == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeFloat(qtd);
        }
    }
}
