package com.example.aulaprogramacaomobile.modelos;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity(
        foreignKeys = @ForeignKey(entity = Tipo.class,
                parentColumns = "id",
                childColumns = "tipoId",
                onUpdate = ForeignKey.CASCADE)
)
public class Refeicao {
    @PrimaryKey(autoGenerate = true)
    private int id;

    private int tipoId;
    private Date data;
    private int hora;
    private int minutos;
    private boolean teveBebida;

    public Refeicao(int tipoId, Date data, int hora, int minutos, boolean teveBebida){
        this.teveBebida = teveBebida;
        this.setTipoId(tipoId);
        this.setData(data);
        this.setHora(hora);
        this.setMinutos(minutos);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTipoId() {
        return tipoId;
    }

    public void setTipoId(int tipoId) {
        this.tipoId = tipoId;
    }

    public Date getData(){
        return data;
    }

    public void setData(Date data){
        this.data = data;
    }

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

    public int getMinutos() {
        return minutos;
    }

    public void setMinutos(int minutos) {
        this.minutos = minutos;
    }

    public boolean isTeveBebida() {
        return teveBebida;
    }

    public void setTeveBebida(boolean teveBebida) {
        this.teveBebida = teveBebida;
    }
}
