package com.example.aulaprogramacaomobile.modelos;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(
        tableName = "tipos",
        indices = @Index(value = {"nome"}, unique = true)
)
public class Tipo {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @NonNull
    private String nome;
    @Nullable
    private String descricao;

    public Tipo(@NonNull String nome, @Nullable String descricao) {
        this.nome = nome;
        this.descricao = descricao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getNome() {
        return nome;
    }

    public void setNome(@NonNull String nome) {
        this.nome = nome;
    }

    @Nullable
    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(@Nullable String descricao) {
        this.descricao = descricao;
    }
}
