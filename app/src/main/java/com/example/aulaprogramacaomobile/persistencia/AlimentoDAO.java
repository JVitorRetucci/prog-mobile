package com.example.aulaprogramacaomobile.persistencia;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.aulaprogramacaomobile.modelos.Alimento;

import java.util.List;

@Dao
public interface AlimentoDAO {

    @Insert
    void insert(Alimento alimento);

    @Delete
    void delete(Alimento alimento);

    @Update
    void update(Alimento alimento);

    @Query("SELECT * FROM alimento WHERE refId = :refId")
    List<Alimento> queryAllByRef(long refId);

    @Query("DELETE FROM alimento WHERE refID = :refId")
    void queryDeleteALlByRef(long refId);
}
