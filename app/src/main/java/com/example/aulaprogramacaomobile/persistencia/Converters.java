package com.example.aulaprogramacaomobile.persistencia;

import androidx.room.TypeConverter;
import java.util.Date;

public class Converters {
    @TypeConverter
    public static Date timeStampParaDate(Long data){
        return data == null ? null : new Date(data);
    }

    @TypeConverter
    public static Long dateParaTimeStamp(Date data){
        return data == null ? null : data.getTime();
    }
}
