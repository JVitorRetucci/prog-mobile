package com.example.aulaprogramacaomobile.persistencia;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.aulaprogramacaomobile.modelos.Refeicao;

import java.util.List;

@Dao
public interface RefeicaoDAO {

    @Insert
    long insert(Refeicao refeicao);

    @Delete
    void delete(Refeicao refeicao);

    @Update
    int update(Refeicao refeicao);

    @Query("SELECT * FROM refeicao WHERE id = :id")
    Refeicao queryForId(long id);

    @Query("SELECT * FROM refeicao ORDER BY data DESC")
    List<Refeicao> queryAll();
}
