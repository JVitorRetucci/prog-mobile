package com.example.aulaprogramacaomobile.persistencia;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.aulaprogramacaomobile.R;
import com.example.aulaprogramacaomobile.modelos.Alimento;
import com.example.aulaprogramacaomobile.modelos.Refeicao;
import com.example.aulaprogramacaomobile.modelos.Tipo;

import java.util.concurrent.Executors;

@Database(entities = {Refeicao.class, Alimento.class, Tipo.class}, version = 3, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class RefeicaoDatabase extends RoomDatabase {
    public abstract RefeicaoDAO refeicaoDAO();
    public abstract AlimentoDAO alimentoDAO();
    public abstract TipoDAO tipoDAO();

    private static RefeicaoDatabase instance;

    public static RefeicaoDatabase getDatabase(final Context context){
        if(instance == null){
            synchronized (RefeicaoDatabase.class){
                if(instance == null){
                    RoomDatabase.Builder builder = Room.databaseBuilder(context,
                                                    RefeicaoDatabase.class,
                                                    "aux_dieta.db")
                                                    .allowMainThreadQueries();

                    builder.addCallback(new Callback() {
                        @Override
                        public void onCreate(@NonNull SupportSQLiteDatabase db) {
                            super.onCreate(db);
                            Executors.newSingleThreadExecutor().execute(new Runnable() {
                                @Override
                                public void run() {
                                    carregaTiposIniciais(context);
                                }
                            });
                        }
                    });

                    instance = (RefeicaoDatabase) builder.build();
                }
            }
        }
        return instance;
    }

    public static void carregaTiposIniciais(final Context context){
        String[] nomes = context.getResources().getStringArray(R.array.tipos_iniciais);

        for (String nome : nomes) {

            Tipo tipo = new Tipo(nome, null);

            instance.tipoDAO().insert(tipo);
        }
    }
}
