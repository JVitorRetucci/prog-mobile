package com.example.aulaprogramacaomobile.persistencia;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.aulaprogramacaomobile.modelos.Refeicao;
import com.example.aulaprogramacaomobile.modelos.Tipo;

import java.util.List;

@Dao
public interface TipoDAO {
    @Insert
    long insert(Tipo tipo);

    @Delete
    void delete(Tipo tipo);

    @Update
    void update(Tipo tipo);

    @Query("SELECT * FROM tipos WHERE id = :id")
    Tipo queryForId(long id);

    @Query("SELECT nome FROM tipos ORDER BY id")
    List<String> queryAllNames();

    @Query("SELECT * FROM tipos ORDER BY id")
    List<Tipo> queryAll();

    @Query("SELECT nome FROM tipos WHERE id = :id")
    String getNameById(int id);
}
