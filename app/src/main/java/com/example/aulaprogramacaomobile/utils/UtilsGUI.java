package com.example.aulaprogramacaomobile.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.example.aulaprogramacaomobile.R;

public class UtilsGUI {
    public static void confirmaAcao(Context contexto, String msg, DialogInterface.OnClickListener listener){
        AlertDialog.Builder builder = new AlertDialog.Builder(contexto);

        builder.setTitle(R.string.confirm_deletion);
        builder.setIcon(android.R.drawable.ic_dialog_alert);

        builder.setMessage(msg);

        builder.setPositiveButton(R.string.yes, listener);
        builder.setPositiveButton(R.string.no, listener);

        AlertDialog alert = builder.create();
        alert.show();
    }
}
